import express from "express";
import path from "path";

const main = () => {
  const app = express();

  app.get("/", (req, res) => {
    res.sendFile(path.join(__dirname, "../", "views/index.html"));
  });

  app.listen(4000, () => {
    console.log("server is listening on port 4000");
  });
};

main();
